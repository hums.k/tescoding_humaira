package com.testcoding.humaira.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testcoding.humaira.dto.BiodataDto;
import com.testcoding.humaira.entity.BiodataEntity;
import com.testcoding.humaira.repository.BiodataRepository;

@RestController
@RequestMapping("/biodata")
public class BiodataController {
	@Autowired
	private BiodataRepository repository;

	@PostMapping("/post")
	public BiodataDto insert(@RequestBody BiodataDto dto) {
		BiodataEntity entity = new BiodataEntity();
		entity = convertToEntity(dto);
		repository.save(entity);
		return dto;
	}

	@GetMapping("/all")
	public List<BiodataDto> getAll() {
		List<BiodataEntity> biodataEntity = repository.findAll();
		List<BiodataDto> biodataDto = biodataEntity.stream().map(this::convertToDto).collect(Collectors.toList());
		return biodataDto;
	}

	@GetMapping("/{id}")
	public BiodataDto getById(@PathVariable Integer id) {
		if (repository.findById(id).isPresent()) {
			BiodataDto bioDto = convertToDto(repository.findById(id).get());
			return bioDto;
		}
		return null;
	}

	@PutMapping("/{id}")
	public BiodataDto update(@RequestBody BiodataDto newBio, @PathVariable Integer id) {
		if (repository.findById(id).isPresent()) {
			BiodataDto bioDto = convertToDto(repository.findById(id).get());
			bioDto.setIdBio(id);
			if (newBio.getNama() != null)
				bioDto.setNama(newBio.getNama());
			if (newBio.getAlamat() != null)
				bioDto.setAlamat(newBio.getAlamat());
			if (newBio.getNoHp() != null)
				bioDto.setNoHp(newBio.getNoHp());

			BiodataEntity bioEntity = new BiodataEntity();
			bioEntity = convertToEntity(bioDto);
			repository.save(bioEntity);
			return bioDto;
		}
		return null;
	}

	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable Integer id) {
		repository.deleteById(id);
	}

	// Convert dto to entity
	public BiodataEntity convertToEntity(BiodataDto dto) {
		BiodataEntity bioEntity = new BiodataEntity();
		bioEntity.setIdBio(dto.getIdBio());
		bioEntity.setNama(dto.getNama());
		bioEntity.setAlamat(dto.getAlamat());
		bioEntity.setNoHp(dto.getNoHp());
		return bioEntity;
	}

	// Convert entity to dto
	public BiodataDto convertToDto(BiodataEntity entity) {
		BiodataDto bioDto = new BiodataDto();
		bioDto.setIdBio(entity.getIdBio());
		bioDto.setNama(entity.getNama());
		bioDto.setAlamat(entity.getAlamat());
		bioDto.setNoHp(entity.getNoHp());
		return bioDto;
	}

}
