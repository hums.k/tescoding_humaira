package com.testcoding.humaira.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BiodataDto {
	private Integer idBio;
	private String nama;
	private String alamat;
	private String noHp;

}
