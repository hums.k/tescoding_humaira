package com.testcoding.humaira.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "t_biodata")
public class BiodataEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_bio")
	private Integer idBio;
	
	@Column(name = "nama", length = 50)
	private String nama;

	@Column(name = "alamat", length = 255)
	private String alamat;

	@Column(name = "nohp", length = 16)
	private String noHp;

}
