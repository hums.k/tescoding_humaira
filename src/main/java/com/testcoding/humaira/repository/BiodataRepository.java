package com.testcoding.humaira.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.testcoding.humaira.entity.BiodataEntity;


@Repository
public interface BiodataRepository extends JpaRepository<BiodataEntity, Integer> {
	
}

