var tableBiodata = {
    create: function () {
        // jika table tersebut datatable, maka clear and dostroy
        if ($.fn.DataTable.isDataTable('#tableBiodata')) {
            //table yg sudah dibentuk menjadi datatable harus d rebuild lagi untuk di instantiasi ulang
            $('#tableBiodata').DataTable().clear();
            $('#tableBiodata').DataTable().destroy();
        }

        $.ajax({
            url: '/biodata/all',
            method: 'get',
            contentType: 'application/json',
            success: function (res, status, xhr) {
                if (xhr.status == 200 || xhr.status == 201) {
                    $('#tableBiodata').DataTable({
                        data: res,
                        columns: [
                            {
                                title: "id",
                                data: "idBio"
                            },
                            {
                                title: "Nama",
                                data: "nama"
                            },
                            {
                                title: "Alamat",
                                data: "alamat"
                            },
                            {
                                title: "No Hp",
                                data: "noHp"
                            }
                        ]
                    });

                } else {

                }
            },
            error: function (err) {
                console.log(err);
            }
        });


    }
};


var formBiodata = {
    resetForm: function () {
        $('#formprofile')[0].reset();
    },

    saveForm: function () {

        var dataResult = getJsonForm($("#formprofile").serializeArray(), true);

        $.ajax({
            url: '/biodata/post',
            method: 'post',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(dataResult),
            success: function (result) {
                console.log(result);
                tableBiodata.create();
                $('#modal-biodata').modal('hide')


            },
            error: function (err) {
                console.log(err);
            }

        }
        );


    }, getBiodata: function (nik) {
        //formBiodata.resetForm();

        if ($.fn.DataTable.isDataTable('#mytable')) {
            //table yg sudah dibentuk menjadi datatable harus d rebuild lagi untuk di instantiasi ulang
            $('#mytable').DataTable().clear();
            $('#mytable').DataTable().destroy();
        }

        $.ajax({
            url: '/person/' + nik,
            method: 'get',
            contentType: 'application/json',
            dataType: 'json',

            success: function (result) {
                //var hasil = JSON.stringify(result),
                console.log(result[0]);

                // var coba = JSON.stringify(result);
                // console.log(coba);
                // console.log(coba.message);

                if (result[0].status == 'true') {
                    $('#mytable').DataTable({
                        "lengthChange": false,
                        "searching": false,
                        "autoWidth": false,
                        "responsive": false,
                        "bPaginate": false,
                        "bSort": false,
                        "bInfo": false,
                        data: [result.data],
                        columns: [
                            {
                                title: "id",
                                data: "idBio"
                            },
                            {
                                title: "Nama",
                                data: "nama"
                            },
                            {
                                title: "Alamat",
                                data: "alamat"
                            },
                            {
                                title: "No Hp",
                                data: "noHp"
                            }
                        ]
                    });

                } else {

                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: result[0].message,
                        showConfirmButton: false,
                        timer: 3000
                    })

                }
            },
            error: function (err) {
                console.log(err);
            }
        });

    }

};
